---
title: "Survival for INCA (and RTR) data"
author: "Erik Bulow"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Survival for INCA (and RTR) data}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---



```{r, echo=FALSE}
options(digits = 2, width = 100)
```




# Intro

This R package aims to simplify analysis and presentation of survival data from INCA. This is done by assumig certain default names of variables and Swedish population mortality data is included in the package. 

The package heavily relies on the "survival" and "relsurv" packages. The recommended workflow also inlude the use of the pipe operator from the "magrittr" package (popularized through the "dplyr" package), although this is not mandatory.


# Data

This vignette (as well as function examples) in the package make extensive use of a test data set (included in the package) called `ex_inca_survdata`:
```{r}
library(incasurv) 
library(magrittr)
knitr::kable(head(ex_inca_survdata))
```

The actual data is just randomly generated with no connection to any real individuals. The structure of the dataset is however intended to mimic real INCA-data (column names are treated as non case sensitive within the package). Hence:

* columns "KON_VALUE", VITALSTATUS" and "VITALSTATUSDATUM_ESTIMAT" should be present
* either "PERSNR" or "PAT_ID" should be present if there might be more than one tumour per patient (otherwise, independense is falsely assumed)
* there should be columns with "age" and "sex" but the names of these columns are arbitrary
* there could be a column called "strata" that is used for stratification but this is not necesary and stratification is also possible without a particular column named "strata"
* additional INCA-columns might be present as well but mostly ignored


## Prepare the data 
As mentioned above, columns with "age" and "sex" are mandatory but their names might differ. The data can be easily prepared for later analysis using `as.incasurvdata`:
```{r}
x <- as.incasurvdata(ex_inca_survdata, diadat = "diadat", age = "age")
```

Object `x` is essentialy still a data.frame but with some new columns later used for survival calculations. It is also given a class attribute "incasurvdata" for method dispatch (you do not need to know what this means) and an attribute `selection_label` with a discription of some basic demographics (which might later be used for a sub title if the result is showed on a plot).

```{r}
str(x)
```




## RTR data

There is also a wrapper function `as.rtrsurvdata` to use with RTR-data that works very similair. See `?as.rtrsurvdata` for details.


# Survival analysis

When we have the data in place, the next step is to perform the survival analysis. This is done by function `incasurv`
 which is used for both observed and (by default) relative survival. Let's here also stratify the resut by the "strata" column.
 
 
```{r, warning = FALSE}
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata")
```

The output of the `incasurv` function is now an object of class `incasurv`. This is handy, since we can thenuse S3 methods for common generics. 



# Summary data

The `incasurv` data can be easily summarised after its calculation. The default is to present survival data for each year for the first five years after diagnosis (see `?summary.incasurv` for details on modification) but we here chose to limit ourselves to the first three years:
```{r, warning=FALSE}
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    summary(times = 0:3)
```




# Plot

To plot the resault is as easy as summarizing it:
```{r, warning=FALSE, fig.width=8, fig.height=8}
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    ggincasurv(times = 0:3)
```





# Customization

The `plot` can be costumized in several ways, for example like this:
```{r, warning=FALSE, fig.width=8, fig.height=8}
library(ggplot2)

ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    ggincasurv(times = 0:3,
    
        # Add confidence intervals
        ci = TRUE, 
        
        # Mark times when censoring occours
        censoring_shape = "|",
    
        # Change the title
        add_to_km = ggtitle("Just an example!"),
    
        # Change background colour of the table
        add_to_table = theme(panel.background = element_rect(fill = "lightblue"))
    )
```

The Kaplan-Meier graph and the table can also be handled separately as gg-objects (known by the "ggplot2" package) by function `plot_km` and `plot.summary.incasurv` (called by `plot` on the output of `summary`).

