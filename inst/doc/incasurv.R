## ---- echo=FALSE----------------------------------------------------------------------------------
options(digits = 2, width = 100)

## -------------------------------------------------------------------------------------------------
library(incasurv) 
library(magrittr)
knitr::kable(head(ex_inca_survdata))

## -------------------------------------------------------------------------------------------------
x <- as.incasurvdata(ex_inca_survdata, diadat = "diadat", age = "age")

## -------------------------------------------------------------------------------------------------
str(x)

## ---- warning = FALSE-----------------------------------------------------------------------------
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata")

## ---- warning=FALSE-------------------------------------------------------------------------------
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    summary(times = 0:3)

## ---- warning=FALSE, fig.width=8, fig.height=8----------------------------------------------------
ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    ggincasurv(times = 0:3)

## ---- warning=FALSE, fig.width=8, fig.height=8----------------------------------------------------
library(ggplot2)

ex_inca_survdata %>% 
    as.incasurvdata(diadat = "diadat", age = "age") %>% 
    incasurv(strata = "strata") %>% 
    ggincasurv(times = 0:3,
    
        # Add confidence intervals
        ci = TRUE, 
        
        # Mark times when censoring occours
        censoring_shape = "|",
    
        # Change the title
        add_to_km = ggtitle("Just an example!"),
    
        # Change background colour of the table
        add_to_table = theme(panel.background = element_rect(fill = "lightblue"))
    )

