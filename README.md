[![Travis-CI Build Status](https://travis-ci.org/cancercentrum/incasurv.svg?branch=master)](https://travis-ci.org/cancercentrum/incasurv)
[![Coverage Status](https://img.shields.io/codecov/c/github/cancercentrum/incasurv/master.svg)](https://codecov.io/github/cancercentrum/incasurv?branch=master)

# incasurv

This package is intended as one of the dependency packages of `rcc`.
It includes functions to work with relative survival data from INCA.

# Install

If not used as a part of the `rcc` package. It could also be downloaded using the 
devtools package:
```{r}
devtools::install_github("cancercentrum/incasurv")
```
