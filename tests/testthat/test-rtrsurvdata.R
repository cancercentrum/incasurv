context("rtrsurvdata")

test_that("rtrsurvdata", {
    expect_error(as.rtrsurvdata(ex_rtr_survdata), 'argument "censoring_date" is missing')
    expect_is(as.rtrsurvdata(ex_rtr_survdata, Sys.Date()), "incasurvdata")
    expect_error(as.rtrsurvdata(ex_inca_survdata, Sys.Date()),
                 '"kön", "ålder", "dödat", "avgm", "avidat"')
    expect_error(as.rtrsurvdata(ex_rtr_survdata, Sys.Date() + 1), 
                 "censoring_date <= Sys.Date")
    
})
