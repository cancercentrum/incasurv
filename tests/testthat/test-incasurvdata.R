context("incasurvdata")

# Problematic data that will generate warnings and errors
ex_inca_survdata <- incasurv::ex_inca_survdata
ex_inca_survdata$age2 <- ex_inca_survdata$age + 30
ex_inca_survdata$age3 <- ex_inca_survdata$age
ex_inca_survdata$age3[19] <- NA
ex_inca_survdata$diadat2 <- ex_inca_survdata$diadat
ex_inca_survdata$diadat2[19] <- NA
ex_inca_survdata$diadat3 <- as.character(ex_inca_survdata$diadat)
ex_inca_survdata$diadat3[19] <- "2020-12-31"

# Data with non unique id
ex_inca_survdata_nonid <- ex_inca_survdata
ex_inca_survdata_nonid$pat_id <- 1:100

test_that("as.incasurvdata", {
    expect_true(all(c("pat_id", "age", "kon_value", "diadat", "strata", "vitalstatus", 
                  "vitalstatusdatum_estimat", "sex", "year", "time", "event") %in%
                    names(as.incasurvdata(ex_inca_survdata))))
    expect_equal(as.incasurvdata(ex_inca_survdata),
                 as.incasurvdata(ex_inca_survdata, diadat = "diadat", age = "age"))
})
    

test_that("errors and warnings", {
    expect_error(as.incasurvdata(iris))
    expect_warning(as.incasurvdata(ex_inca_survdata, age = "age2"),
                   "Some people seems to be very old: ")
    expect_warning(as.incasurvdata(ex_inca_survdata, age = "age3"),
                   " cases with missing age dropped")
    expect_warning(as.incasurvdata(ex_inca_survdata, diadat = "diadat2"),
                   " cases with missing diadat dropped!")
    expect_warning(as.incasurvdata(ex_inca_survdata, diadat = "diadat3"),
                   "Unvalid diadat")
    expect_warning(as.incasurvdata(ex_inca_survdata_nonid), 
                   "Patients with multiple tumors")
})


test_that("attributes", {
    expect_is(as.incasurvdata(ex_inca_survdata), "incasurvdata")
    expect_true(is.incasurvdata(as.incasurvdata(ex_inca_survdata)))
    expect_false(is.incasurvdata(iris))
    expect_equivalent(attr(as.incasurvdata(incasurv::ex_inca_survdata), "selection_label"), 
                 "Urval: ålder [1 - 100 år], kön [kvinnor, män] och år [2005 - 2010]")

})
