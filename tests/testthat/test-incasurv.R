context("incasurv")


x <- as.incasurvdata(incasurv::ex_inca_survdata)
y <- x[, -5]

test_that("incasurv", {
    expect_equal(names(incasurv(x)), 
                 c("n", "time", "n.risk", "n.event", "n.censor", "std.err", "surv", 
                   "strata", "lower", "upper", "conf.type", "conf.int", "method", 
                   "call", "type"))
    expect_message(incasurv(x), "column 'strata' used for stratification!")
    expect_warning(incasurv(x, strata = "kon_value"), 
                   "Original column 'strata' replaced by kon_value as specified by argument 'strata'!")
    expect_is(incasurv(x), "rs.surv")
    expect_is(incasurv(x), "incasurv")
    expect_true(is.incasurv(incasurv(x)))
    expect_false(inherits(incasurv(x, method = "observed"), "rs.surv"))
    expect_error(incasurv(x, method = "qewfqev"))
    expect_equal(attr(incasurv(x), "strata_label"), "strata")
    expect_equal(attr(incasurv(y), "strata_label"), "")
    expect_null(incasurv(y)$strata)
    expect_equal(incasurv(x)$strata, 
                 structure(c(107L, 95L, 85L, 86L, 98L), .Names = c("a", "e", "d", "c", "b")))
    
})
