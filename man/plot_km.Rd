% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/plot_km.R
\name{plot_km}
\alias{plot_km}
\title{Kaplan-Meier plot for INCA-data}
\usage{
plot_km(x, times = 0:5, time_unit = "year", ci = FALSE,
  extra_sub_title = "", censoring_shape = NULL)
}
\arguments{
\item{x}{object of type \code{\link{incasurv}}}

\item{times}{vector of times; the returned matrix will contain 1 row for each time.
The vector will be sorted into increasing order; missing values are not allowed.
If censored = TRUE, the default times vector contains all the unique times in fit,
otherwise the default times vector uses only the event (death) times.}

\item{time_unit}{specifies the unit of what is given by argument \code{times};
one of "year", "month", "week" or "day"}

\item{ci}{should the plot have 95 \% confidence intervals (\code{FALSE} by default)}

\item{extra_sub_title}{text (character string of length one) to append to
the automatically generated subtitle. Tho completely change the title,
add a layer made by \code{\link{ggtitle}} to the returned object instead}

\item{censoring_shape}{marker for censored observations. \code{NULL} (as default)
means no marks, otherwise valid shapes as described in \code{\link{aes_linetype_size_shape}}.}
}
\value{
Object of type "gtable". (Use the "plot" function to show the graph if "plot_km" called
directly (not through \code{\link{plot.incasurv}}).)
}
\description{
A Kaplan-Meier plot of survival curves is produced, one curve for each strata.
Curves are presented in alphabetical order.
}
\examples{
library(magrittr)
library(ggplot2)

x <- ex_inca_survdata \%>\%
     as.incasurvdata() \%>\%
     incasurv()

plot_km(x, 0:3)
plot_km(x, 0:3, ci = TRUE)
plot_km(x, 0:3, censoring_shape = "|",  extra_sub_title = ". Note: fake data!")

# Modify the lines
plot_km(x, 0:3) + geom_step(aes(colour = strata, size = 2))
}

