% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/plot.incasurv.summary.R
\name{plot.summary.incasurv}
\alias{plot.summary.incasurv}
\title{Plot method for \code{incasurv.summary} objects}
\usage{
\method{plot}{summary.incasurv}(x, text_size = 3, right_margin = 1,
  left_margin = 1, ...)
}
\arguments{
\item{x}{object of type \code{\link{summary.incasurv}}}

\item{text_size}{text size modifier}

\item{right_margin}{adjust the right margon}

\item{left_margin}{adjust the left margins}

\item{...}{ignored}
}
\description{
Graphical representation of table data from \code{\link{summary.incasurv}}.
}
\examples{
library(magrittr)
x <-
 ex_inca_survdata \%>\%
 as.incasurvdata() \%>\%
 incasurv() \%>\%
 summary()

plot(x)
plot(x, 10)
}

