#' Create Survival Curves for INCA (or RTR/Rockan) data
#' 
#' Essentially a wrapper around either \code{\link{rs.surv}} or \code{\link{survfit}} adapted to INCA
#' data.
#' 
#' @param x object of type \code{incasurvdata} (see \code{\link{as.incasurvdata}})
#' @param strata character vector of names of columns in \code{x} that should be used for stratification of the data.
#' Set to \code{NULL} (as default) for no stratification if no column in \code{x} named strata. If there is a column in \code{x} named "strata",
#' this column will however be used for stratification if \code{strata = NULL} (drop or rename the column to avoid stratification).
#' @param popmort ratetable passed to \code{\link{rs.surv}}. The latest available 
#' table for Swedish population (if included in the package) is used by default.
#' @param method either "observed" to calculate observed survival (see \code{\link{survfit}}) 
#' or a method for relative survival passed to \code{\link{rs.surv}}. \code{ederer2} by default.
#' @param ... arguments passed to \code{\link{rs.surv}}.
#' 
#' @return 
#' 
#' \code{as.incasurv} returns object of class \code{incasurv} with some attributes intended mostly for internal use. 
#' Attribute "method" however reports the argument \code{method}.
#' The class also inherits (and the object gains extra attributes) from either \code{\link{survfit}} 
#' (if argument \code{method} set to "observed") or \code{\link{rs.surv}} otherwise. 
#' 
#' \code{is.incasurv} returns \code{TRUE} if object is of class "incasurv" and \code{FALSE} otherwise.
#' @export
#' 
#' @examples 
#' x <- as.incasurvdata(ex_inca_survdata)
#' incasurv(x)
#' is.incasurv(x)

incasurv <- function(x, strata = NULL, popmort = default_popmort(), method = "ederer2", ...) {
    
    stopifnot(is.incasurvdata(x))
    x <- rccmisc::lownames(x)
    x <- create_stratum_var(strata, x)
   
    # Fit the model (either observed or relative, with or without stratas)
    fit <-  if (method == "observed") {
                if (is.null(x$strata)) {
                    survival::survfit(survival::Surv(time, event) ~ 1, data = x)  
                } else {
                    survival::survfit(survival::Surv(time, event) ~ strata, data = x)     
                }
            } else {
                if (is.null(x$strata)) {
                    relsurv::rs.surv(survival::Surv(time, event) ~ 1,
                                      data = x, ratetable = popmort, method = method, ...)
                } else {
                    relsurv::rs.surv(survival::Surv(time, event) ~ strata,
                                     data = x, ratetable = popmort, method = method, ...)
                }
            }

    ## Stratum names for relative survival are coded in a non pleasent way but changed here 
    ## to readable form 
    if (!is.null(x$strata)) {
        if (method == "observed") { 
            
            names(fit$strata) <- gsub("strata=", "", names(fit$strata), fixed = TRUE) 
        
        } else {
          
            a <- strsplit(names(fit$strata), "strata", fixed = TRUE)
            a <- unlist(lapply(a, function(x){
                x <- gsub(", ", "", x)
                x <- x[substring(x, nchar(x)) == 1]
                if (identical(x, character(0))) NA else x
            }))
            a <- gsub("=1", "", a)
            
            ## The first level of fit is coded as all equals 0 and has to be 
            ## taken back from the opne unused level of x$strata!
            a[is.na(a)] <- levels(x$strata)[!(levels(x$strata) %in% a)]
            names(fit$strata) <- a
        }
    }
    
    structure(fit, 
              class           = unique(c("incasurv", class(fit))),
              method          = method,
              strata_label    = if (is.null(strata) & !is.null(x$strata)) "strata" else paste( strata, collapse = ", "),
              selection_label = attr(x, "selection_label"))
}


#' @rdname incasurv
#' @export
is.incasurv <- function(x) {
    inherits(x, "incasurv")
}
