#' Plot method for incasurv objects
#' 
#' A Kaplan-Meier plot of survival curves is produced, one curve for each strata.
#' Curves are presented in alphabetical order.
#' The graph is also annotated by a table with additional information 
#' (to make the Kaplan-Meier plot without the table, use function \code{\link{plot_km}} instead).
#' 
#' 
#' @inheritParams summary.incasurv 
#' @param x object of class \code{incasurv} (see \code{\link{incasurv}})
#' @param times argument passed to \code{\link{summary.incasurv}}
#' @param ... arguments passed to \code{\link{plot_km}}
#' @param table_args additional arguments (as named list) passed to \code{\link{plot.summary.incasurv}}
#' @param add_to_km object of class gg/ggplot to add to the Kaplan Meier plot produced by \code{\link{plot_km}}. 
#' This can be used for example to add a layer or to change a title etcetera. 
#' @param add_to_table object of class gg/ggplot to add to the summary table under the Kaplan-Meier plot produced by \code{\link{plot.summary.incasurv}}. This can be used for example to add a layer or to change a title etcetera. 
#' @param show Should the result be diaplayed graphically (\code{TRUE} by default). 
#' If \code{FALSE} an object of class "gtable" is returned.
#' 
#' @return Object of class gtable.
#' @export
#' @name plot.incasurv
#' 
#' @examples
#' library(magrittr)
#' library(ggplot2)
#' 
#' x <- ex_inca_survdata %>% 
#'      as.incasurvdata() %>% 
#'      incasurv()
#'      
#' x <- incasurv(as.incasurvdata(ex_inca_survdata))
#' ggincasurv(x) 
#' 
#' # Only the first 3 years
#' ggincasurv(x, 0:3)
#' 
#' # Add confidence intervals
#' ggincasurv(x, 0:3, ci = TRUE)
#' 
#' # Change the title
#' ggincasurv(x, 0:3, add_to_km = ggtitle("Just an example!"))
#' 
#' # Change background colour of the table
#' ggincasurv(x, 0:3, add_to_table = theme(panel.background = element_rect(fill = "lightblue")))
#' 
ggincasurv <- function(x, times = 0:5, time_unit = "year", ..., table_args = list(), 
                       add_to_km = NULL, add_to_table = NULL, show = TRUE) {
   
    table_args$x <- summary(x, times = times, time_unit = time_unit)

    p <- 
        gridExtra::arrangeGrob(
        plot_km(x, times, ...) + add_to_km, 
        do.call(plot, table_args) + add_to_table, 
        clip = FALSE, nrow = 2, ncol = 1, 
        heights = grid::unit(c(2, .5), c("null", "null"))
    )
    if (show) plot(p)
    invisible(p)
}


