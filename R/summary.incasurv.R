
#' Summarise survival data from an incasurv object
#' 
#' @inheritParams survival::summary.survfit
#' @param object object of type \code{\link{incasurv}}
#' @param time_unit specifies the unit of what is given by argument \code{times};
#' one of "year", "month", "week" or "day"
#' @param ci_digits number of digits iused in confidence intervals 
#' (represented as a static character that can not be changed later using \code{\link{format}})
#' @param ... arguments passed to \code{\link{summary.survfit}}
#' @export
#' 
#' @return Object of class "incasurv.summary" with some extra attributes. 
#' The object is basically a data.frame with column names matching the output of
#' \code{\link{summary.survfit}}
#' 
#' @examples 
#' library(magrittr)
#' 
#' x <- ex_inca_survdata %>% 
#'    as.incasurvdata() %>% 
#'    incasurv() %>% 
#'    summary()
summary.incasurv <-
    function(object, times = 0:5, time_unit = "year", 
             ci_digits = 0, censored = FALSE, ...){
   
    # Transform times to days
    time_in_days <- time_in_days(times, time_unit)
    
    ##  Get the data - times argument should be missing if !censored,
    ##  I could not solve thos any other way than using do.call
    args <- list(...)
    args$object     <- object
    args$censored   <- censored
    if (!censored) {
        args$times  <- time_in_days$times 
        args$scale  <- time_in_days$scale
    }
    args$extend     <- TRUE
    rs_sum <- do.call(NextMethod, args)
    
    ## Take all columns that can be fit into a data frame
    out <- data.frame(rs_sum[c("time", "n.risk", "n.event", "n.censor", 
                               "surv", "std.err", "lower", "upper")])
    if (!is.null(object$strata)) {
        out$strata <- factor(rs_sum$strata, levels = sort(levels(rs_sum$strata)))
    }
    
    ## Include confidence intervall if specified
    out$ci <- ifelse(out$time == 0, NA,
                     paste0("(", round(rs_sum$lower * 100, ci_digits), "-", 
                         round(rs_sum$upper * 100, ci_digits), ")"))
    
    # Reorder rows and columns
    col_order_strata_time <- if (!is.null(object$strata)) c("strata", "time") else "time"
    out <- out[do.call(order, out[col_order_strata_time]), unique(c(col_order_strata_time, names(out)))]
        
    ##  Set saome attributes with additional information
    structure(out,
              class     = unique(c("summary.incasurv", "AsIs", class(out))),
              conf.int  = rs_sum$conf.int,
              time_unit = time_unit,
              type      = rs_sum$type,
              method    = attr(object, "method"),
              selection_label = attr(object, "selection_label"),
              strata_label  = attr(object, "strata_label")
    )
}





is.incasurv.summary <- function(x){
    inherits(x, "summary.incasurv")
}
