
#' Alias for default popmort table
#' 
#' The default popmort table might change over time (yearly updates).
#' Instead of specifying the name of a specific popmort-table, this 
#' function can be used to access the default one.
#' The popmort table was last updated in 2014.
#' 
#' @return Object of class \code{\link{ratetable}}
#' @export
default_popmort <- function() {
    incasurv::popmort19582014
}


# Helper function to create id from data.frame that hopefully has a column 
# named "persnr", "pnr" or "pat_id"
create_id <- function(x) {
    ne <- function(name) name %in% names(x)
    id <- if (ne("persnr")) {
        x$persnr 
    } else if (ne("pat_id")) {
        x$pat_id 
    } else if (ne("pnr")) {
        x$pnr 
    } else {
        warning("Personal identification number missing in dataset! All rows assumed independent! ", 
                "Please supply vector named 'persnr' or 'pat_id' if this is not the case!")
        seq_len(nrow(x))
    }
    id
}


# Create a column in x named "strata" as a factor variable with strata
# based on variable name specified in strata_names
create_stratum_var <- function(strata_names, x) {
 
    # Handle situation if column strata already exists
    if (!is.null(x$strata)) {
        if (all(is.na(x$strata)) || length(unique(x$strata[!is.na(x$strata)])) == 1) {
            warning("column 'strata' has less than two unique non-NA-values and is therefore ignored!")
            x$strata <- NULL
        } else if (is.null(strata_names)) { 
            message("column 'strata' used for stratification!")
            x$strata <- as.factor(as.character(x$strata))   
            return(x)
        } else if (!is.null(strata_names)) {
            warning("Original column 'strata' replaced by ", 
                    paste(strata_names, collapse = ", "), 
                    " as specified by argument 'strata'!")  
        }  
    } 
    
    if (!is.null(strata_names)) {
      
      strata_names <- tolower(strata_names)
      
      if ("kon_value" %in% strata_names && "kon_value_beskrivning" %in% names(x)) {
          strata_names[strata_names == "kon_value"] <- "kon_value_beskrivning"
      } else if (!all(strata_names %in% names(x))) {
          stop("Unvalid 'strata' argument! Must be a vector with all elements corresponding to names of columns in 'x'!")
      }
      
      ## Use specified stratum variable if only one specified
      if (length(strata_names) == 1) {
          x$strata <- x[[strata_names]]
          
          ## If strata is a vector, strata is taken as an interaction
      } else {
          ## If there is a strata column that should be used as one of the strata,
          ## columns, it must be character to allow new values in it
          if (!is.null(x$strata)) x$strata <- as.character(x$strata) 
          x$strata <- interaction(x[strata_names], sep = ", ")
      }
      strata_names_label <- paste(strata_names, collapse = ", ")
      x$strata <- 
          if (all(is.na(x$strata)) || length(unique(x$strata[!is.na(x$strata)])) == 1) {
            warning("column(s) ", strata_names_label, 
                    " has less than two unique non-NA-values and is therefore ignored!")
            NULL
          } else {
            message("column ", strata_names_label, " used for stratification!")
            as.factor(as.character(x$strata))
          }
    }
  x
}



# Colors to use in the graph
colors_hc <- function() {
    c("#00b3f6","#ffb117", "#434348", 
        "#90ed7d","#AAEEEE", "#f15c80",
        "#e4d354", "#2b908f", "#f45b5b",
        "#7798BF", "#8085e9")
}


# Translate times in specified unit to times in days
time_in_days <- function(times, time_unit) {
    if ( !(time_unit %in% c("year", "month", "week", "day")) ) {
        stop("Unvalid time unit. Must be one of 'year', 'month', 'week' or 'day'!")
    }
    dy <- 365.24
    scale <- switch(time_unit, year = dy, month = dy / 12, week = dy / 52, day = 1)
    list(times = times * scale, scale = scale)
}

# Translate from english to Swedish for labels etc
translate_time_unit <- function(time_unit) {
    switch(time_unit, 
           year = "\u00c5r",
           month = "M\u00e5nader",
           week = "Veckor",
           day = "Dagar",
           stop(time_unit, "is not a recognised time unit!")
    )
}

